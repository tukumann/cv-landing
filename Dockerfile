# FROM node:10.13-alpine as node-angular-cli

# LABEL authors="riene"

FROM node:latest as build
WORKDIR /app
COPY package*.json ./
RUN npm ci
RUN npm i -g @angular/cli
COPY . .
RUN npm run build --configuration=production
FROM nginx:latest
COPY ./nginx.conf /etc/nginx/conf.d/default/conf
COPY --from=build /app/dist/cv-landing/browser /usr/share/nginx/html
EXPOSE 80
RUN ls -la


#Linux setup
# RUN apk update \
#   && apk add --update alpine-sdk \
#   && apk del alpine-sdk \
#   && rm -rf /tmp/* /var/cache/apk/* *.tar.gz ~/.npm \
#   && npm cache verify \
#   && sed -i -e "s/bin\/ash/bin\/sh/" /etc/passwd

#Angular CLI
#RUN npm install -g @angular/cli

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {
  provideHttpClient,
  withInterceptorsFromDi,
} from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { PersonalInfoComponent } from './components/personal-info/personal-info.component';

const appRoutes: Routes = [
  {
    path: 'personal-info',
    component: PersonalInfoComponent,
  },
  // { path: 'hero/:id', component: HeroDetailComponent },
  // {
  //     path: 'heroes',
  //     component: HeroListComponent,
  //     data: { title: 'Heroes List' },
  // },
  // { path: '', redirectTo: '/heroes', pathMatch: 'full' },
//   { path: 'pi', redirectTo: '/personal-info', pathMatch: 'full' },
//   { path: 'pi', redirectTo: '/personal-info', pathMatch: 'full' },
//   { path: '**', component: PersonalInfoComponent },
];

@NgModule({
  declarations: [AppComponent],
  bootstrap: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    RouterModule.forRoot(appRoutes),
  ],
  providers: [provideHttpClient(withInterceptorsFromDi())],
})
export class AppModule {}

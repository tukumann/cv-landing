import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IData } from './interfaces/IData';
import { animate, style, transition, trigger } from '@angular/animations';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    trigger('inOutEaseAnimation', [
      transition(
        ':enter',
        [
          style({ opacity: 0.15 }),
          animate('{{duration}}s ease-out', style({ opacity: 1 })),
        ],
        { params: { duration: '1.6' } }
      ),
      transition(
        ':leave',
        [
          style({ opacity: 1 }),
          animate('{{duration}}s ease-in', style({ opacity: 0.15 })),
        ],
        { params: { duration: '1.6' } }
      ),
    ]),
  ],
})
export class AppComponent {
  public title = 'cv-landing';
  public data: IData[] = [];
  showImage = true;

  constructor(private http: HttpClient) {
    this.http.get<any>('assets/db.json').subscribe((data) => {
      this.data = data?.data?.reverse();
    });
  }

  onClick() {
    this.showImage = !this.showImage;
  }
}
